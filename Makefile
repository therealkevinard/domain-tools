BINDIR := $(CURDIR)/bin
PKG := gitlab.com/therealkevinard/domain-tools

.PHONY: build
build:
	go build -o $(BINDIR)/domain-tools

install:
	go install $(PKG)

deps_build:
	go build -o $(BINDIR)/domain-tools github.com/bobesa/go-domain-util/cmd/domainparser
	go generate github.com/bobesa/go-domain-util/domainutil
