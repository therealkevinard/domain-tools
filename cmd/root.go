/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	homedir "github.com/mitchellh/go-homedir"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/therealkevinard/domain-tools/pkg/logger"
	"os"
)

var cfgFile string
var domainsFile string
var domains []string
var logFormat string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "domain-tools",
	Short: "cli tool for analyzing dns records",
	Long: `checks for common dns records and whois status.
optionally resolves cname records. 
attempts to use cloudflare api when necessary.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	//	Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		logger.Log.Error(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.domain-tools.yaml)")
	rootCmd.PersistentFlags().StringSliceVarP(&domains, "domains-list", "l", []string{}, "target domain(s). single domain name or comma-separated list of domain names to target")
	rootCmd.PersistentFlags().StringVarP(&domainsFile, "domains-file", "f", "", "read domains from file")
	rootCmd.PersistentFlags().StringVarP(&logFormat, "output-format", "o", "json", "output format")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		logger.Log.Out = os.Stdout
		logger.Log.SetFormatter(&logrus.JSONFormatter{})

		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			logger.Log.Error(err)
			os.Exit(1)
		}

		viper.AddConfigPath(home)
		viper.SetConfigName(".domain-tools")
	}

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		logger.Log.Debug("Using config file:", viper.ConfigFileUsed())
	}

	// read env vars
	viper.SetEnvPrefix("DT")
	viper.AutomaticEnv()
	logger.Log.Debug("config loaded")
}
