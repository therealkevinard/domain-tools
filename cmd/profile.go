/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"bufio"
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/therealkevinard/domain-tools/pkg/logger"
	"gitlab.com/therealkevinard/domain-tools/pkg/profiler"
	"os"
)

var followCname bool

var profileCmd = &cobra.Command{
	Use:   "profile",
	Short: "Fetch registration/host details for domain name(s)",
	Long: `Lookup information such as IP Address, Nameservers (dig-style), and key Info from the whois database. Includes support for cloudflare API
Usage:

- profile a single domain 
domain-tools profile -l site.com

- profile a list of domains. provide a csl of domain names to -l 
domain-tools profile -l site.com,www.site.com

- don't resolve cnames (boolean: must use eq. sign for -c=false) 
domain-tools profile -c=false -l site.com

- read domains list from lines in a file (one domain per line) 
domain-tools profile -f /path/to/file.txt
`,
	Run: func(cmd *cobra.Command, args []string) {
		switch logFormat {
		case "json":
			logger.Log.SetFormatter(&logrus.JSONFormatter{})
			break
		case "txt":
			logger.Log.SetFormatter(&logrus.TextFormatter{})
			break
		default:
			logger.Log.Info("output-format must be json|txt, falling-back to default json format")
			logger.Log.SetFormatter(&logrus.JSONFormatter{})
		}

		// @todo: make -f and -l args mutually exclusive to reduce code complexity.
		// read lines from file
		if domainsFile != "" && len(domains) == 0 {
			file, err := os.Open(domainsFile)
			if err != nil {
				fmt.Println(err.Error())
			}
			if file != nil && len(domains) == 0 {
				defer file.Close()
				scanner := bufio.NewScanner(file)
				for scanner.Scan() {
					domains = append(domains, scanner.Text())
				}
			}
		}

		// neither -f or -l gave us domains
		if len(domains) == 0 {
			logger.Log.Panic("No domains found: either -f should be a valid domains file, or -l should contain one or more domains")
		}


		for _, domain := range domains {
			p, err := profiler.New(domain, followCname)
			if err != nil {
				logger.Log.WithFields(logrus.Fields{"domain": domain, "message": "host lookup failed. not creating profiler"}).Error(err.Error())
			} else {
				err := p.Profile()
				if err != nil {
					logger.Log.WithFields(logrus.Fields{"domain": p.Domain}).Error(err)
				} else {
					logger.Log.WithFields(logrus.Fields{"domain": p.Domain, "profile": p.Result}).Info("profile completed")
				}
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(profileCmd)
	profileCmd.Flags().BoolVarP(&followCname, "follow-cname", "c", true, "follow cnames. if true, first checks the domain for a cname record and runs all tests against the target domain")
}
