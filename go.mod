module gitlab.com/therealkevinard/domain-tools

go 1.14

require (
	github.com/bobesa/go-domain-util v0.0.0-20190911083921-4033b5f7dd89
	github.com/cloudflare/cloudflare-go v0.11.7
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/google/go-cmp v0.4.1 // indirect
	github.com/likexian/whois-go v1.6.1
	github.com/likexian/whois-parser-go v1.14.4
	github.com/mitchellh/go-homedir v1.1.0
	github.com/mitchellh/mapstructure v1.3.1 // indirect
	github.com/pelletier/go-toml v1.8.0 // indirect
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/cobra v1.0.0
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.7.0
	golang.org/x/sys v0.0.0-20200519105757-fe76b779f299 // indirect
	gopkg.in/ini.v1 v1.56.0 // indirect
)
