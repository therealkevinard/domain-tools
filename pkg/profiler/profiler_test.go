package profiler

import (
	"fmt"
	"testing"
)

// @todo: passing assertion control to the test spec is _technically_ okay,
// 		but may be cleaner to use something like github.com/google/go-cmp/cmp for evaluating in the "proper" t.Run
func TestNew(t *testing.T) {
	type args struct {
		domain string
		follow bool
	}

	tests := []struct {
		name     string
		args     args
		wantEval bool
		eval     func(t *testing.T, got *Profiler, err error)
		wantErr  bool
	}{
		{
			name:     "simple case",
			args:     args{domain: "site.com", follow: true},
			wantEval: true,
			eval: func(t *testing.T, got *Profiler, err error) {
				want := &Profiler{Domain: "site.com"}
				if got.Domain != want.Domain {
					t.Errorf("New() got = %v, want %v", got.Domain, want.Domain)
				}
			},
		},
		{
			name: "invalid domain name",
			args: args{
				domain: "invalid-domain-name",
				follow: true,
			},
			wantEval: true,
			eval: func(t *testing.T, got *Profiler, err error) {
				if got != nil {
					t.Errorf("Incorrectly created profiler: %v", got)
				}

				expectErr := fmt.Sprintf("lookup %v: no such host", "invalid-domain-name")
				if err.Error() != expectErr {
					t.Errorf("expected error: '%v', got '%v'", expectErr, err.Error())
				}

			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := New(tt.args.domain, tt.args.follow)
			if tt.wantEval {
				tt.eval(t, got, err)
				return // definitely return: all assertions are handled in tt.evals
			}

			if (err != nil) != tt.wantErr {
				t.Errorf("New() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

		})
	}
}

func TestProfiler_CheckCloudflare(t *testing.T) {
	type fields struct {
		Domain string
		Result *ProfileResult
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Profiler{
				Domain: tt.fields.Domain,
				Result: tt.fields.Result,
			}
			if err := p.CheckCloudflare(); (err != nil) != tt.wantErr {
				t.Errorf("CheckCloudflare() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestProfiler_EvalCnameRecord(t *testing.T) {
	type fields struct {
		Domain string
		Result *ProfileResult
	}
	tests := []struct {
		name        string
		fields      fields
		wantChanged bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Profiler{
				Domain: tt.fields.Domain,
				Result: tt.fields.Result,
			}
			if gotChanged := p.EvalCnameRecord(); gotChanged != tt.wantChanged {
				t.Errorf("EvalCnameRecord() = %v, want %v", gotChanged, tt.wantChanged)
			}
		})
	}
}

func TestProfiler_Profile(t *testing.T) {
	type fields struct {
		Domain string
		Result *ProfileResult
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Profiler{
				Domain: tt.fields.Domain,
				Result: tt.fields.Result,
			}
			if err := p.Profile(); (err != nil) != tt.wantErr {
				t.Errorf("Profile() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_getPreferredConfig(t *testing.T) {
	type args struct {
		keys []string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getPreferredConfig(tt.args.keys); got != tt.want {
				t.Errorf("getPreferredConfig() = %v, want %v", got, tt.want)
			}
		})
	}
}
