package profiler

import (
	"github.com/bobesa/go-domain-util/domainutil"
	"github.com/cloudflare/cloudflare-go"
	"github.com/likexian/whois-go"
	whoisparser "github.com/likexian/whois-parser-go"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"net"
	"strings"
)

type Profiler struct {
	Domain string         `json:"target_domain"`
	Result *ProfileResult `json:"profile_result"`
}

type ProfileResult struct {
	// cname
	CnameChanged struct {
		From    string `json:"from,omitempty"`
		To      string `json:"to,omitempty"`
		Changed bool   `json:"changed,omitempty"`
	} `json:"cname_changed"`

	// ip address
	IpAddr    string `json:"ip_lookup,omitempty"`
	IpAddrErr string `json:"ip_lookup_error,omitempty"`

	// nameservers
	Ns    *NsResult    `json:"ns"`
	Whois *WhoisResult `json:"whois"`
}
type WhoisResult struct {
	Domain      string `json:"domain"`
	WhoisServer string `json:"whois_server"`
	Registrar   string `json:"registrar"`
	Error       string `json:"error"`
}
type NsResult struct {
	NameServers []string `json:"nameservers"`
	Error       string   `json:"error"`
}

func New(domain string, follow bool) (*Profiler, error) {
	p := &Profiler{Domain: domain}
	host, err := net.LookupHost(p.Domain)
	if host == nil {
		return nil, err
	}

	if follow && p.EvalCnameRecord() {
		p.Domain = p.Result.CnameChanged.To
	}

	return &Profiler{
		Domain: domain,
		Result: new(ProfileResult),
	}, nil
}

func (p *Profiler) EvalCnameRecord() (changed bool) {
	// Check for cname. If the domain follows, change the profile domain to the target, and record the change in CnameChanged
	cname, err := net.LookupCNAME(p.Domain)
	if err != nil {
		log.Error(err.Error())
	}

	cname = strings.TrimSuffix(cname, ".")

	cnameChanged := len(cname) > 0 && cname != p.Domain //
	if cnameChanged {
		p.Result.CnameChanged.From = p.Domain
		p.Result.CnameChanged.To = cname
	}

	return cnameChanged
}

func (p *Profiler) Profile() error {
	// ip addr
	ip, err := net.LookupIP(p.Domain)
	if err != nil {
		p.Result.IpAddrErr = err.Error()
	}
	p.Result.IpAddr = ip[0].String()

	// only run some checks on "main" domain.
	// @todo: for subdomains, should we run checks on main instead?
	// 		probably - a use where foo.site.com is a separately-hosted service, we will need to fully resolve the home for
	// 		foo.site.com, ignorant of site.com
	isSubdomain := domainutil.HasSubdomain(p.Domain) //@todo: this is a single-use pkg dep. consider using rex or similar in place of pkg;
	if isSubdomain {
		return nil
	}
	log.WithFields(log.Fields{"Domain": p.Domain}).Debug(p.Domain, "looking up ns, whois")

	// whois lookup
	whois_raw, err := whois.Whois(p.Domain)
	if err != nil {
		p.Result.Whois = &WhoisResult{Error: err.Error()}
	}
	parsed, err := whoisparser.Parse(whois_raw)
	if err != nil {
		p.Result.Whois = &WhoisResult{Error: err.Error()}
	} else {
		p.Result.Ns = &NsResult{
			NameServers: parsed.Domain.NameServers,
		}
		p.Result.Whois = &WhoisResult{
			Domain:      parsed.Domain.Domain,
			WhoisServer: parsed.Domain.WhoisServer,
			Registrar:   parsed.Registrar.Name,
		}
	}

	// check cloudflare
	isCloudflareDomain := strings.Contains(
		strings.Join(p.Result.Ns.NameServers, ","),
		"cloudflare.com",
	)

	if isCloudflareDomain {
		if err := p.CheckCloudflare(); err != nil {
			// the cloudflare client doesn't seem to return many errors - more empty fields, and move along. try anyway.
			log.WithFields(log.Fields{"Domain": p.Domain}).Debug(p.Domain, "cf-api lookup failed")
		}
	}
	return nil
}

// iterator for loading config keys by preference.
// tries 0..Max, so most preferred should be later in the slice
func getPreferredConfig(keys []string) string {
	var config string
	for _, key := range keys {
		if len(viper.GetString(key)) > 0 {
			config = viper.GetString(key)
		}
	}
	return config
}

func (p *Profiler) CheckCloudflare() error {
	log.WithFields(log.Fields{"Domain": p.Domain}).Debug(p.Domain, "detected cloudflare nameservers. attempting cf-api lookup")

	// load config; pref: env, config file
	key := getPreferredConfig([]string{"providers.cloudflare.api_token", "CF_API_TOKEN"})

	// @todo: there will very likely be rate limit errors here, but I haven't encountered yet.
	api, apiErr := cloudflare.NewWithAPIToken(key)
	if apiErr != nil {
		return apiErr
	}

	// get zone id from cf
	zoneId, zoneErr := api.ZoneIDByName(p.Domain)
	if zoneErr != nil {
		return zoneErr
	}

	// filter zone records: return a recs for targeted domain name
	dns, dnsErr := api.DNSRecords(zoneId, cloudflare.DNSRecord{Name: p.Domain, Type: "A"})
	if dnsErr != nil {
		return dnsErr
	}
	p.Result.IpAddr = dns[0].Content

	return nil
}
